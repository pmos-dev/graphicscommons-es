/*
 * Public API Surface of ngx-graphicscommons-es-core
 */

export * from './lib/components/graphics-svg/graphics-svg.component';

export * from './lib/ngx-graphicscommons-es-core.module';
