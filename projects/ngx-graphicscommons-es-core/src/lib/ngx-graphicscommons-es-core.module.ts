import { NgModule } from '@angular/core';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';

import { GraphicsSvgComponent } from './components/graphics-svg/graphics-svg.component';

@NgModule({
		imports: [
				NgxAngularCommonsEsCoreModule
		],
		declarations: [
				GraphicsSvgComponent
		],
		exports: [
				GraphicsSvgComponent
		]
})
export class NgxGraphicsCommonsEsCoreModule {}
