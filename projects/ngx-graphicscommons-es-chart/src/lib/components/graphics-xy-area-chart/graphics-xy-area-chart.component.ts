import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { refactorScale, isAllScaleValuesSameType } from '../../helpers/scales';

import { TGraphicsCommonsScale } from '../../types/tgraphicscommons-scale';
import { TRefactoredScale, TRefactorResult } from '../../types/trefactored-scale';

@Component({
		selector: 'graphics-xy-area-chart',
		templateUrl: './graphics-xy-area-chart.component.html',
		styleUrls: ['./graphics-xy-area-chart.component.less']
})
export class GraphicsXyAreaChartComponent extends CommonsComponent implements OnChanges {
	@Input() width: number = 100;
	@Input() height: number = 100;
	@Input() leftIndent: number = 0.1;
	@Input() rightIndent: number = 0.02;
	@Input() topIndent: number = 0.02;
	@Input() bottomIndent: number = 0.1;
	@Input() scaleXTextOffset: number = 0.01;
	@Input() scaleYTextOffset: number = 0.01;
	@Input() scaleX: TGraphicsCommonsScale[] = [];
	@Input() scaleY: TGraphicsCommonsScale[] = [];
	@Input() scaleLineColor: string = '#e0e0e0';
	@Input() scaleStrokeWidth: number = 0.002;
	@Input() scaleTextColor: string = '#666666';
	@Input() scaleTextSize: number = 0.02;
	@Input() minX?: number;
	@Input() maxX?: number;
	@Input() minY?: number;
	@Input() maxY?: number;
	@Input() showScaleX: boolean = true;
	@Input() showScaleY: boolean = true;
	@Input() showScaleXTicks: boolean = true;
	@Input() showScaleYTicks: boolean = true;
	@Input() showScaleXValues: boolean = true;
	@Input() showScaleYValues: boolean = true;
	@Input() gridLineColor: string = '#f0f0f0';
	@Input() gridStrokeWidth: number = 0.001;
	@Input() showGridX: boolean = false;
	@Input() showGridY: boolean = false;

	internalScaleX: TRefactoredScale[] = [];
	internalScaleY: TRefactoredScale[] = [];
	internalMinX: number = 0;
	internalMaxX: number = 0;
	internalMinY: number = 0;
	internalMaxY: number = 0;

	getScaleXLinePath(): string {
		return `M${this.leftIndent * this.width} ${this.height * (1 - this.bottomIndent)} H ${this.width}`;
	}

	getScaleYLinePath(): string {
		return `M${this.leftIndent * this.width} 0 V ${this.height * (1 - this.bottomIndent)}`;
	}

	getScaleXTickPath(position: number): string {
		const x: number = this.width * position * (1 - (this.leftIndent + this.rightIndent));
		return `M${this.leftIndent * this.width} ${this.height * (1 - this.bottomIndent)} m ${x} 0 v 0 ${this.width * 0.01}`;
	}

	getScaleYTickPath(position: number): string {
		const y: number = this.height * (1 - position) * (1 - (this.topIndent + this.bottomIndent));
		return `M${this.leftIndent * this.width} ${this.topIndent * this.height} m 0 ${y} h -${this.width * 0.01} 0`;
	}

	getScaleXTextX(position: number): number {
		return (this.leftIndent * this.width) + (this.width * position * (1 - (this.leftIndent + this.rightIndent)));
	}

	getScaleXTextY(): number {
		return (1 - (this.bottomIndent - this.scaleXTextOffset)) * this.height;
	}

	getScaleYTextX(): number {
		return (this.leftIndent - this.scaleYTextOffset) * this.width;
	}

	getScaleYTextY(position: number): number {
		return (this.topIndent * this.height) + (this.height * (1 - position) * (1 - (this.topIndent + this.bottomIndent)));
	}

	getGridXPath(position: number): string {
		const x: number = this.width * position * (1 - (this.leftIndent + this.rightIndent));
		return `M${this.leftIndent * this.width} ${this.height * (1 - this.bottomIndent)} m ${x} 0 V 0`;
	}

	getGridYPath(position: number): string {
		const y: number = this.height * (1 - position) * (1 - (this.topIndent + this.bottomIndent));
		return `M${this.leftIndent * this.width} ${this.topIndent * this.height} m 0 ${y} H ${this.width}`;
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.scaleX) {
			this.refactorScaleX();
		}
		if (changes.scaleY) {
			this.refactorScaleY();
		}
	}

	private refactorScaleX(): void {
		if (!isAllScaleValuesSameType(this.scaleX)) throw new Error('Multi-type for scaleX');

		const result: TRefactorResult = refactorScale(
				this.scaleX,
				this.minX,
				this.maxX
		);

		this.internalScaleX = result.internalScale;
		this.minX = result.min;
		this.maxX = result.max;
	}

	private refactorScaleY(): void {
		if (!isAllScaleValuesSameType(this.scaleY)) throw new Error('Multi-type for scaleY');
		
		const result: TRefactorResult = refactorScale(
				this.scaleY,
				this.minY,
				this.maxY
		);

		this.internalScaleY = result.internalScale;
		this.minY = result.min;
		this.maxY = result.max;
	}
}
