import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { commonsTypeIsArray } from 'tscommons-es-core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { refactorScale, isAllScaleValuesSameType } from '../../helpers/scales';
import { refactorPoints } from '../../helpers/datas';

import { TGraphicsCommonsScale } from '../../types/tgraphicscommons-scale';
import { TRefactoredScale, TRefactorResult } from '../../types/trefactored-scale';
import { TGraphicsCommonsData } from '../../types/tgraphicscommons-data';
import { TRefactoredPoint } from '../../types/trefactored-point';

type TInternalData = Pick<TGraphicsCommonsData, 'color'> & {
		refactoredPoints: TRefactoredPoint[];
};

@Component({
		selector: 'graphics-xy-line-chart',
		templateUrl: './graphics-xy-line-chart.component.html',
		styleUrls: ['./graphics-xy-line-chart.component.less']
})
export class GraphicsXyLineChartComponent extends CommonsComponent implements OnChanges {
	@Input() width: number = 100;
	@Input() height: number = 100;
	@Input() leftIndent: number = 0.1;
	@Input() rightIndent: number = 0.02;
	@Input() topIndent: number = 0.02;
	@Input() bottomIndent: number = 0.1;
	@Input() scaleXTextOffset: number = 0.01;
	@Input() scaleYTextOffset: number = 0.01;
	@Input() scaleX: TGraphicsCommonsScale[] = [];
	@Input() scaleY: TGraphicsCommonsScale[] = [];
	@Input() scaleLineColor: string = '#e0e0e0';
	@Input() scaleStrokeWidth: number = 0.002;
	@Input() scaleTextColor: string = '#666666';
	@Input() scaleTextSize: number = 0.02;
	@Input() minX?: number;
	@Input() maxX?: number;
	@Input() minY?: number;
	@Input() maxY?: number;
	@Input() showScaleX: boolean = true;
	@Input() showScaleY: boolean = true;
	@Input() showScaleXTicks: boolean = true;
	@Input() showScaleYTicks: boolean = true;
	@Input() showScaleXValues: boolean = true;
	@Input() showScaleYValues: boolean = true;
	@Input() gridLineColor: string = '#f0f0f0';
	@Input() gridStrokeWidth: number = 0.001;
	@Input() showGridX: boolean = false;
	@Input() showGridY: boolean = false;
	
	@Input() data: TGraphicsCommonsData|TGraphicsCommonsData[] = [];
	@Input() pointRadius: number = 0.005;
	@Input() showPoints: boolean = true;
	@Input() showLines: boolean = false;
	@Input() lineStrokeWidth: number = 0.003;

	internalDatas: TInternalData[] = [];
	internalScaleX: TRefactoredScale[] = [];
	internalScaleY: TRefactoredScale[] = [];
	internalMinX: number = 0;
	internalMaxX: number = 0;
	internalMinY: number = 0;
	internalMaxY: number = 0;

	getPointX(position: number): number {
		const x: number = this.width * position * (1 - (this.leftIndent + this.rightIndent));
		return (this.leftIndent * this.width) + x;
	}

	getPointY(position: number): number {
		const y: number = this.height * (1 - position) * (1 - (this.topIndent + this.bottomIndent));
		return (this.topIndent * this.height) + y;
	}

	getLinePath(point: TRefactoredPoint, last: TRefactoredPoint): string {
		const x1: number = this.width * last.positionX * (1 - (this.leftIndent + this.rightIndent));
		const y1: number = this.height * (1 - last.positionY) * (1 - (this.topIndent + this.bottomIndent));
		const x2: number = this.width * point.positionX * (1 - (this.leftIndent + this.rightIndent));
		const y2: number = this.height * (1 - point.positionY) * (1 - (this.topIndent + this.bottomIndent));
		
		return `M${this.leftIndent * this.width} ${this.topIndent * this.height} m ${x1} ${y1} l ${x2 - x1} ${y2 - y1}`;
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.scaleX) {
			this.refactorScaleX();
		}
		if (changes.scaleY) {
			this.refactorScaleY();
		}
		if (changes.data) {
			this.refactorDatas();
		}
	}

	private refactorDatas(): void {
		const ds: TGraphicsCommonsData[] = commonsTypeIsArray(this.data) ? this.data : [ this.data ];

		this.internalDatas = ds
			.map((d: TGraphicsCommonsData): TInternalData => ({
					color: d.color,
					refactoredPoints: refactorPoints(
							d.points,
							this.internalScaleX,
							this.internalScaleY,
							this.minX,
							this.maxX,
							this.minY,
							this.maxY
					)
			}));
	}

	private refactorScaleX(): void {
		if (!isAllScaleValuesSameType(this.scaleX)) throw new Error('Multi-type for scaleX');

		const result: TRefactorResult = refactorScale(
				this.scaleX,
				this.minX,
				this.maxX
		);

		this.minX = result.min;
		this.maxX = result.max;
		this.internalScaleX = result.internalScale;
	}

	private refactorScaleY(): void {
		if (!isAllScaleValuesSameType(this.scaleY)) throw new Error('Multi-type for scaleY');
		
		const result: TRefactorResult = refactorScale(
				this.scaleY,
				this.minY,
				this.maxY
		);

		this.minY = result.min;
		this.maxY = result.max;
		this.internalScaleY = result.internalScale;
	}
}
