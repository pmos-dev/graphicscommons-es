import { TGraphicsCommonsXyPoint } from './tgraphicscommons-xy-point';

export type TGraphicsCommonsData = {
		color: string;
		points: TGraphicsCommonsXyPoint[];
};
