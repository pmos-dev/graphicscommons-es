import { TGraphicsCommonsScale } from './tgraphicscommons-scale';

export type TRefactoredScale = TGraphicsCommonsScale & Required<Pick<TGraphicsCommonsScale, 'position'>>;

export type TRefactorResult = {
		internalScale: TRefactoredScale[];
		min: number|undefined;
		max: number|undefined;
};
