export type TGraphicsCommonsXyPoint = {
		x: number|string|Date;
		y: number|string|Date;
};
