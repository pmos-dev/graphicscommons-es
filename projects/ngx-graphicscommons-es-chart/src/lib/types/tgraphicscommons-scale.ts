export type TGraphicsCommonsScale = {
		value: number|string|Date;
		position?: number;
		pretty?: string;
};
