import { TGraphicsCommonsXyPoint } from './tgraphicscommons-xy-point';

export type TRefactoredPoint = TGraphicsCommonsXyPoint & {
		positionX: number;
		positionY: number;
};
