import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsString } from 'tscommons-es-core';

import {
		CommonsPrettyDateTimePipe,
		CommonsPrettyDatePipe,
		CommonsPrettyTimePipe,
		CommonsPrettyIntegerPipe,
		CommonsPrettyPercentagePipe
} from 'ngx-angularcommons-es-pipe';

import { TGraphicsCommonsScale } from '../types/tgraphicscommons-scale';

@Pipe({
		name: 'scale'
})
export class ScalePipe implements PipeTransform {
	transform(scale: TGraphicsCommonsScale): string {
		if (!scale.pretty || commonsTypeIsString(scale.value)) return scale.value.toString();

		switch (scale.pretty) {
		case 'integer':
			return new CommonsPrettyIntegerPipe().transform(scale.value, true);
		case 'percent':
			return new CommonsPrettyPercentagePipe().transform(scale.value, true);
		case 'datetime':
			return new CommonsPrettyDateTimePipe().transform(scale.value, true, true) as string;
		case 'date':
			return new CommonsPrettyDatePipe().transform(scale.value, true, true) as string;
		case 'time':
			return new CommonsPrettyTimePipe().transform(scale.value, true, true) as string;
		}

		return 'Unknown pretty pipe';
	}

}
