import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxGraphicsCommonsEsCoreModule } from 'ngx-graphicscommons-es-core';

import { GraphicsXyAreaChartComponent } from './components/graphics-xy-area-chart/graphics-xy-area-chart.component';
import { GraphicsXyLineChartComponent } from './components/graphics-xy-line-chart/graphics-xy-line-chart.component';

import { ScalePipe } from './pipes/scale.pipe';

@NgModule({
		imports: [
				CommonModule,
				NgxGraphicsCommonsEsCoreModule
		],
		declarations: [
				GraphicsXyAreaChartComponent,
				GraphicsXyLineChartComponent,
				ScalePipe
		],
		exports: [
				GraphicsXyLineChartComponent
		]
})
export class NgxGraphicsCommonsEsChartModule { }
