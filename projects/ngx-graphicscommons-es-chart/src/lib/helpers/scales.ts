import {
		commonsTypeIsNumber,
		commonsTypeIsString,
		commonsTypeIsDate
} from 'tscommons-es-core';

import { TGraphicsCommonsScale } from '../types/tgraphicscommons-scale';
import { TRefactoredScale, TRefactorResult } from '../types/trefactored-scale';

export function getScaleValueTypes(scale: TGraphicsCommonsScale[]): string[] {
	const types: string[] = [];
	for (const s of scale) {
		const type: string = typeof s.value;
		if (!types.includes(type)) types.push(type);
	}

	return types;
}

export function isAllScaleValuesSameType(scale: TGraphicsCommonsScale[]): boolean {
	const types: string[] = getScaleValueTypes(scale);
	return types.length < 2;
}

export function refactorScale(
		src: TGraphicsCommonsScale[],
		min: number|undefined,
		max: number|undefined
): TRefactorResult {
	if (src.length === 0) {
		return {
				internalScale: [],
				min: min,
				max: max
		};
	}

	const internalScale: TRefactoredScale[] = src
		.map((scale: TGraphicsCommonsScale, index: number): TRefactoredScale => {
			const clone: TGraphicsCommonsScale = { ...scale };

			if (clone.position === undefined) {
				if (commonsTypeIsNumber(clone.value)) clone.position = clone.value as number;
				if (commonsTypeIsString(clone.value)) clone.position = index;
				if (commonsTypeIsDate(clone.value)) clone.position = clone.position = clone.value.getTime();
			}

			if (clone.position === undefined) throw new Error('Unknown position type');
			return {
					...clone,
					position: clone.position
			};
		});

	if (min === undefined) {
		for (const scale of internalScale) {
			if (min === undefined || scale.position! < min) min = scale.position;
		}
	}
	if (max === undefined) {
		for (const scale of internalScale) {
			if (max === undefined || scale.position! > max) max = scale.position;
		}
	}

	for (const scale of internalScale) {
		scale.position = (scale.position - min!) / (max! - min!);
	}

	return {
			internalScale: internalScale,
			min: min,
			max: max
	};
}
