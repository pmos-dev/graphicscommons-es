import { commonsTypeIsString, commonsTypeIsNumber, commonsTypeIsDate } from 'tscommons-es-core';

import { TGraphicsCommonsXyPoint } from '../types/tgraphicscommons-xy-point';
import { TRefactoredScale } from '../types/trefactored-scale';
import { TRefactoredPoint } from '../types/trefactored-point';

function getPosition(
		value: string | number | Date,
		scale: TRefactoredScale[],
		min: number|undefined,
		max: number|undefined
): number {
	if (commonsTypeIsString(value)) {
		const match: TRefactoredScale|undefined = scale
			.find((s: TRefactoredScale): boolean => s.value === value);
		if (!match) throw new Error('No matching scale value for that string');

		return match.position;
	}
	
	if (min === undefined || max === undefined) throw new Error('Min/max for a number or Date are unset');

	if (commonsTypeIsNumber(value)) {
		return (value - min) / (max - min);
	}

	if (commonsTypeIsDate(value)) {
		const time: number = value.getTime();
		return (time - min) / (max - min);
	}

	throw new Error('Unknown data type');
}

export function refactorPoints(
		points: TGraphicsCommonsXyPoint[],
		scaleX: TRefactoredScale[],
		scaleY: TRefactoredScale[],
		minX: number|undefined,
		maxX: number|undefined,
		minY: number|undefined,
		maxY: number|undefined
): TRefactoredPoint[] {
	if (scaleX.length === 0 || scaleY.length === 0) return [];
	
	return points
		.map((point: TGraphicsCommonsXyPoint): TRefactoredPoint => ({
				...point,
				positionX: getPosition(point.x, scaleX, minX, maxX),
				positionY: getPosition(point.y, scaleY, minY, maxY)
		}));
}
