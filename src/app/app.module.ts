import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxGraphicsCommonsEsCoreModule } from 'ngx-graphicscommons-es-core';
import { NgxGraphicsCommonsEsChartModule } from 'ngx-graphicscommons-es-chart';

import { AppComponent } from './app.component';

@NgModule({
		imports: [
				BrowserModule,
				NgxGraphicsCommonsEsCoreModule,
				NgxGraphicsCommonsEsChartModule
		],
		declarations: [
				AppComponent
		],
		providers: [],
		bootstrap: [ AppComponent ]
})
export class AppModule { }
