import { Component } from '@angular/core';

import { commonsDateYmdToDate } from 'tscommons-es-core';

import { TData } from 'ngx-graphicscommons-es-chart';

@Component({
		selector: 'app-root',
		templateUrl: './app.component.html',
		styleUrls: ['./app.component.less']
})
export class AppComponent {
	title = 'GraphicsCommonsEs';

	scaleX = [
			{
					value: commonsDateYmdToDate('2021-05-01'),
					pretty: 'date'
			},
			{
					value: commonsDateYmdToDate('2021-05-03'),
					pretty: 'date'
			},
			{
					value: commonsDateYmdToDate('2021-05-04'),
					pretty: 'date'
			},
			{
					value: commonsDateYmdToDate('2021-05-06'),
					pretty: 'date'
			}
	];

	scaleY = [
			{
					value: 3
			},
			{
					value: 5
			},
			{
					value: 6
			},
			{
					value: 10
			}
	];

	data: TData[] = [
			{
					color: '#ff0000',
					points: [
							{ x: commonsDateYmdToDate('2021-05-01'), y: 4 },
							{ x: commonsDateYmdToDate('2021-05-02'), y: 7 },
							{ x: commonsDateYmdToDate('2021-05-03'), y: 9 },
							{ x: commonsDateYmdToDate('2021-05-05'), y: 8 },
							{ x: commonsDateYmdToDate('2021-05-06'), y: 5 }
					]
			},
			{
					color: '#a0ff00',
					points: [
							{ x: commonsDateYmdToDate('2021-05-01'), y: 3 },
							{ x: commonsDateYmdToDate('2021-05-03'), y: 3 },
							{ x: commonsDateYmdToDate('2021-05-04'), y: 4 },
							{ x: commonsDateYmdToDate('2021-05-05'), y: 6 }
					]
			}
	];
}
